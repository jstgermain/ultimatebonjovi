				<!-- Start Profile -->			
				<div class="three columns">
				    <div class="img-wrp">
		    			<img alt="" class="scaleimg" src="images/team/profile3.jpg" />
		    			<div class="overlay-wrp">
		        			<div class="overlay"></div>
		        			<ul class="social-icons overlay-content">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-linkedin"></i></a></li>
								<li><a href="#"><i class="icon-pinterest"></i></a></li>
		        			</ul>
		    			</div>
				    </div>
					<h3>Member Name</h3>
					<p class="job-position">Bass Guitar</p> 
					<?php /*<p class="description">Aliquam erat volutpat. Nullam placerat enim nec quam dapibus ac egestas massa pulvinar. Nunc in sapien vitae orci viverra lacinia luctus id sapien. Mauris ut nisi at tortor molestie rhoncus ultrices quis nisi. Sed massa sapien, dapibus vel porta non, congue id nisl.</p>*/ ?>
				</div>
				<!-- End Profile -->
				
				<!-- Start Profile -->
				<div class="three columns">
				    <div class="img-wrp">
		    			<img alt="" class="scaleimg" src="images/team/profile1.jpg" />
		    			<div class="overlay-wrp">
		        			<div class="overlay"></div>
		        			<ul class="social-icons overlay-content">
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-linkedin"></i></a></li>
								<li><a href="#"><i class="icon-pinterest"></i></a></li>
		        			</ul>
		    			</div>
				    </div>
					<h3>Member Name</h3>
					<p class="job-position">Drums</p> 
					<?php /*<p class="description">Lorem ipsum dolor sit amet, consectetur <a href="#homepage">adipiscing elit</a>. Maecenas ac augue at erat hendrerit dictum. Praesent porta, purus eget sagittis imperdiet, nulla mi ullamcorper metus, id hendrerit metus diam vitae est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>*/ ?>
				</div>
				<!-- End Profile -->	
				
				<!-- Start Profile -->			
				<div class="three columns">
				    <div class="img-wrp">
		    			<img alt="" class="scaleimg" src="images/team/profile2.jpg" />
		    			<div class="overlay-wrp">
		        			<div class="overlay"></div>
		        			<ul class="social-icons overlay-content">
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-linkedin"></i></a></li>
		        			</ul>
		    			</div>
				    </div>
					<h3>Member Name</h3>
					<p class="job-position">Keyboard</p> 
					<?php /*<p class="description">Phasellus a nisi urna, facilisis facilisis diam. Vivamus enim ligula, sollicitudin nec porttitor nec, commodo vel justo. Nunc in mattis ipsum. Mauris accumsan pretium diam, sit amet iaculis purus ullamcorper ac. Donec pulvinar metus a ipsum varius suscipit. Mauris ut nisi at tortor molestie rhoncus ultrices quis nisi.</p>*/ ?>
				</div>
				<!-- End Profile -->	
				
