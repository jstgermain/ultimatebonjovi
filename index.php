<?php

require_once("Classes/Autoloader.php");

$common = new CommonFunctions();
$twitter = new Twitter\MyClass('5vH4XAyDKbYwtbCFwe1Ew', 'SWHtP1gmajO8UFlg5OJc4rbQj5MEUX2jN7Ud31qXHE', '22924197-lUOrvT7hokDx8GOrwjCzdjnRbwGdq9T9bDoFYZGl8', 'HFXL4DemhAk95RNWJFu94l1ZbVK58WRsBex0cCJxc');

?>

<!DOCTYPE HTML>
<head>
	<meta charset="UTF-8">
	<title><?=$common->siteName;?></title>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Web Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,600' rel='stylesheet' type='text/css'>
	
	<!-- CSS Responsive Framework Skeleton -->
	<link rel="stylesheet" href="css/skeleton.css">
	
	<!-- CSS Styles -->	
	<link rel="stylesheet" href="css/style.css">
	
	<!-- Fullscreen Slider -->
	<link rel="stylesheet" href="css/supersized.css">
	<link rel="stylesheet" href="css/supersized.shutter.css">
	
	<!-- Fancybox -->	
	<link rel="stylesheet" href="css/jquery.fancybox.css">
	
	<!-- Icons -->	
	<link rel="stylesheet" href="css/font-awesome.css">
	
	<!-- Flexslider -->	
	<link rel="stylesheet" href="css/flexslider.css">
	
	<!-- Media Queries -->	
	<link rel="stylesheet" href="css/media.css">
	
	<!-- Twitter Plugin CSS -->	
	<link rel="stylesheet" href="css/twitter.css">
	
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<link rel="stylesheet" href="css/style.design3.css">
	<link rel="stylesheet" href="css/timelinexml.sleek.css">

	<!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

	<!-- All JavaScript at the bottom, except this Modernizr build incl. Respond.js
	Respond is a polyfill for min/max-width media queries. Modernizr enables HTML5 elements & feature detects;
	for optimal performance, create your own custom Modernizr build: www.modernizr.com/download/ -->
	<script src="js/libs/modernizr-2.0.6.min.js"></script>
	
</head>

<body>

	<!-- Start Homepage -->
	<?php include('Views/Home.php'); ?>
	<!-- End Homepage -->
	
	
	<!-- Start Navigation -->
	<?php include('Views/Navigation.php'); ?>
	<!-- End Navigation -->	
	
	
	<!-- Start About Page -->	
	<?php include('Views/About.php'); ?>
	<!-- End About Page -->
	
	
	<!-- Start Shows Page -->
	<?php include('Views/Shows.php'); ?>
	<!-- End Shows Page --> 
	
	
	<!-- Start Portfolio Page -->
	<?php include('Views/Portfolio.php'); ?>
	<!-- End Portfolio Page -->

	
	<!-- Start Contact Page -->
	<?php include('Views/Contact.php'); ?>
	<!-- End Contact Page -->
	
	<!-- JavaScripts -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="js/jquery.easing.min.js"></script>
	<script src="js/supersized.3.2.7.min.js"></script>
	<script src="js/supersized.shutter.min.js"></script>
	<script src="js/supersized.images.js"></script>
	<script src="js/main.js"></script>
	<script src="js/jquery.parallax-1.1.3.js"></script>
	<script src="js/last-tw.js"></script>
	<script src="js/paralax-ini.js"></script>
	<script src="js/scroll.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/selectnav.min.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/jquery.smartresize.js"></script>
	<script src="js/shortcodes.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/contact.js"></script>
	<script>
	    $(window).load(function(){
	      $("nav").sticky({ topSpacing: 0, className: 'sticky', wrapperClassName: 'my-wrapper' });
	    });
    </script>
	
	
	<!-- JavaScript at the bottom for fast page loading -->
	
	<!-- scripts concatenated and minified via build script -->
	<script defer src="js/plugins.js"></script>
	<script defer src="js/mylibs/timelinexml.js"></script>
	<script defer src="js/script.js"></script>
	<!-- end scripts -->
	
	
	<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
	chromium.org/developers/how-tos/chrome-frame-getting-started -->
	<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
	<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->
		
	<script>
		$(function(){
			$('.item').show();
			var $container = $('#portfolio_items');
			$container.isotope({
				itemSelector : '.item'
			});
			var $optionSets = $('.option-set'),
				$optionLinks = $optionSets.find('a');
			$optionLinks.click(function(){
				var $this = $(this);
				// don't proceed if already selected
				if ( $this.hasClass('selected') ) {
					return false;
				}
				var $optionSet = $this.parents('.option-set');
				$optionSet.find('.selected').removeClass('selected');
				$this.addClass('selected');
			// make option object dynamically, i.e. { filter: '.my-filter-class' }
			var options = {},
				key = $optionSet.attr('data-option-key'),
				value = $this.attr('data-option-value');
				
			// parse 'false' as false boolean
			value = value === 'false' ? false : value;
			options[ key ] = value;
				if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
				// changes in layout modes need extra logic
				changeLayoutMode( $this, options )
			} else {
				// otherwise, apply new options
				$container.isotope( options );
			}    
			return false;
			});
			
			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
				var $isMobile = true;
			}
			
			if(!$isMobile)
			{
				$(window).smartresize(function(){
					$container.isotope({
						masonry: { columnWidth: $container.width() / 12 }
					});
				});
			}
			else
			{
				$(window).bind('orientationchange', function(e){
					$container.isotope({
						masonry: { columnWidth: $container.width() / 12 }
					});
				});
			}
		});
	</script>
	
	<script type="text/javascript">
    	function moveTo(contentArea){
        	var goPosition = $(contentArea).offset().top;
        	$('html,body').animate({ scrollTop: goPosition}, 'slow');
        }
    </script>
    
    <script>
		selectnav('nav', {
			nested: true,
			indent: '-'
		});
    </script>

	<script>
		$(function(){
			//SyntaxHighlighter.all();
			});
				$(window).load(function(){
				$('.flexslider').flexslider({
				animation: "slide",
				start: function(slider){
				  $('body').removeClass('loading');
				}
			});
		});
	</script>
    
    <script>
		$(document).ready(function() {
			$(".fancybox").fancybox({
				padding : 0,
/*
				beforeShow: function () {
				
					this.title = $(this.element).attr('title');
					this.title = '<h4>' + this.title + '</h4>' + $(this.element).find('img').attr('alt');
    				
		            if (this.title) {
		                // New line
		                this.title += '<br />';
		                
		                // Add tweet button
		                this.title += '<a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-url="' + this.href + '">Tweet</a> ';
		                
		                // Add FaceBook like button
		                this.title += '<iframe src="//www.facebook.com/plugins/like.php?href=' + this.href + '&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:23px;" allowTransparency="true"></iframe>';
		            }
				},
*/
		        afterShow: function() {
		            // Render tweet button
		            twttr.widgets.load();
		        },
				helpers : {
					title : { type: 'inside' },
				}
			});
			
			$('.fancybox-media').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					media : {},
				}
			});
			
			// Launch Promo Vieo on Page Load
			//$("#promoVideo").trigger('click');
			
		});
	</script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-45093530-1', 'ultimatebonjovi.com');
ga('send', 'pageview');
</script>
		
</body>
<!-- The End -->