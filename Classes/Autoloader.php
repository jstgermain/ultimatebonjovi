<?php
class Autoloader {
    
    static public function loader($className) {
    	
        $filename = 'Classes/' . str_replace('\\', '/', $className) . '.php';
        
        //echo $filename . '</br>';
        
        if (file_exists($filename)) {
        	
            include($filename);
            
            if (class_exists($className)) {
                return TRUE;
            }
            
        }
        
        return FALSE;
        
    }
    
}

spl_autoload_register('Autoloader::loader');
