<?php

//namespace Classes;

class CommonFunctions {
	
	public $siteName	= 'Ultimate Bon Jovi';
	public $facebook	= 'thebonjovitribute';
	public $twitter		= 'ultimatebonjovi';
	public $youtube		= 'nickkoclanis';
	
	public function __construct() {
		$this->facebook	= 'http://www.facebook.com/' . $this->facebook;
		$this->twitter	= 'http://www.twitter.com/' . $this->twitter;
		$this->youtube	= 'http://www.youtube.com/user/' . $this->youtube;
	}
	
}
