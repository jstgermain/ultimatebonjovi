<?php

namespace Twitter;

class MyClass extends Twitter {
	
	public function __construct($tweetAuth=null, $tweetSecret=null, $oAuth=null, $oAuthSecret=null) {
		
		// Construct Twitter Parent Class and sent twitter authorization
		parent::__construct($tweetAuth, $tweetSecret);
		
		// Set oAuth Settings
		$this->setOAuthToken($oAuth);
		$this->setOAuthTokenSecret($oAuthSecret);
		
	}
	
	function twitter_messages($tweet=null, $list = false) {
		
		$num = count($tweet);
		
		if ($list) echo "<ul id=\"twitter_update_list\" style=\"display: block;\">";
		
		for ($i = 0; $i <= $num-1; $i++){
			
			//Assign feed to $feed
			$feed = $tweet[($i)]['text'];
			$msg_date = date("D M d H:i:s +0000 Y",strtotime($tweet[($i)]['created_at']));
			
			//Find location of @ in feed
			$feed = str_pad($feed, 3, ' ', STR_PAD_LEFT);   //pad feed     
			$startat = stripos($feed, '@'); 
			$numat = substr_count($feed, '@');
			$numhash = substr_count($feed, '#'); 
			$numhttp = substr_count($feed, 'http'); 
			
			$feed = preg_replace("/(http:\/\/)(.*?)\/([\w\.\/\&\=\?\-\,\:\;\#\_\~\%\+]*)/", "<a href=\"\\0\" class=\"twitter-link\" target=\"_blank\">\\0</a>", $feed);
			$feed = preg_replace("(@([a-zA-Z0-9\_]+))", "<a href=\"http://www.twitter.com/\\1\" class=\"twitter-link\" target=\"_blank\">\\0</a>", $feed);
			$feed = preg_replace('/(^|\s)#(\w+)/', '\1<a href="http://search.twitter.com/search?q=%23\2" class=\"twitter-link\" target=\"_blank\">#\2</a>', $feed);
			
			$currentTime=date("D M d H:i:s +0000 Y");
			$timeArray = $this->timeDiff($msg_date,$currentTime);
			
			//echo $msg_date . ' ' . $currentTime;
			
			$elapsed = $this->getTimePassed($timeArray['years'], $timeArray['days'], $timeArray['hours'], $timeArray['minutes'], $timeArray['seconds']);
			
			//echo $elapsed;
			
			if ($list) echo	"<li class=\"twitter-item\">\n"; elseif ($num != 1) echo '<p class="twitter-message">';
			echo	"	{$feed}<br/>\n";
			echo	"	<span class=\"tweet_date\">{$elapsed}</span>\n";
			if ($list) echo	"</li>\n"; elseif ($num != 1) echo '</p>';
			
		}
		
		if ($list) echo	"</ul>";
		
	}
	
	// GET TIME DIFFERENCE FROM POST TIME TO NOW
	
	function timeDiff($firstTime,$lastTime) {
	
		// convert to unix timestamps
		$firstTime=strtotime($firstTime);
		$lastTime=strtotime($lastTime);
	
		// perform subtraction to get the difference (in seconds) between times
		$timeDiff=$lastTime-$firstTime;
	
		// return the difference
		return $this->Sec2Time($timeDiff);
	}
	
	//Usage :
	//echo timeDiff("2002-04-16 10:00:00","2002-03-16 18:56:32");
	
	function Sec2Time($time){
		
		if(is_numeric($time)){
			
			$value = array( "years" => 0, "days" => 0, "hours" => 0, "minutes" => 0, "seconds" => 0, );
			
			if($time >= 31556926){
				$value["years"] = floor($time/31556926);
				$time = ($time%31556926);
			}
			
			if($time >= 86400){
				$value["days"] = floor($time/86400);
				$time = ($time%86400);
			}
			
			if($time >= 3600){
				$value["hours"] = floor($time/3600);
				$time = ($time%3600);
			}
			
			if($time >= 60){
				$value["minutes"] = floor($time/60);
				$time = ($time%60);
			}
		
			$value["seconds"] = floor($time);
			
			return (array) $value;
			
		} else {
		
			return (bool) FALSE;
			
		}
		
	}
	
	function getTimePassed($years, $days, $hours, $minutes, $seconds) {
		
		//echo "Years {$years}, Days {$days}, Hours {$hours}, Minutes {$minutes}, Seconds {$seconds}";
		$extender = '';
		
		if($years >= 1) {
			if($years > 1) {
				$extender = 's';
			}
			$timePassed = "{$years} year{$extender}";
		} elseif($days >= 1 && $days <= 364) {
			if($days > 1) {
				$extender = 's';
			}
			$timePassed = "{$days} day{$extender}";
		} elseif($hours >= 1 && $hours <= 23) {
			if($hours > 1) {
				$extender = 's';
			}
			$timePassed = "{$hours} hour{$extender}";
		} elseif($minutes >= 1 && $minutes <= 59) {
			if($minutes > 1) {
				$extender = 's';	
			}
			$timePassed = "{$minutes} minute{$extender}";
		} elseif($seconds >= 0 && $seconds <= 59) {
			if($seconds > 1) {
				$extender = 's';
			}
			$timePassed = "{$seconds} second{$extender}";
		}
		
		return "{$timePassed} ago";
	
	}
	
	// Link discover stuff
	
	function hyperlinks($text) {
	
	    // Props to Allen Shaw & webmancers.com
	
	    // match protocol://address/path/file.extension?some=variable&another=asf%
	    //$text = preg_replace("/\b([a-zA-Z]+:\/\/[a-z][a-z0-9\_\.\-]*[a-z]{2,6}[a-zA-Z0-9\/\*\-\?\&\%]*)\b/i","<a href=\"$1\" class=\"twitter-link\">$1</a>", $text);
	    $text = preg_replace('/\b([a-zA-Z]+:\/\/[\w_.\-]+\.[a-zA-Z]{2,6}[\/\w\-~.?=&%#+$*!]*)\b/i',"<span><a href=\"$1\" class=\"twitter-link\">$1</a></span>", $text);
	
	    // match www.something.domain/path/file.extension?some=variable&another=asf%
	    //$text = preg_replace("/\b(www\.[a-z][a-z0-9\_\.\-]*[a-z]{2,6}[a-zA-Z0-9\/\*\-\?\&\%]*)\b/i","<a href=\"http://$1\" class=\"twitter-link\">$1</a>", $text);
	    $text = preg_replace('/\b(?<!:\/\/)(www\.[\w_.\-]+\.[a-zA-Z]{2,6}[\/\w\-~.?=&%#+$*!]*)\b/i',"<span><a href=\"http://$1\" class=\"twitter-link\">$1</a></span>", $text);    
	    
	    // match name@address
	    $text = preg_replace("/\b([a-zA-Z][a-zA-Z0-9\_\.\-]*[a-zA-Z]*\@[a-zA-Z][a-zA-Z0-9\_\.\-]*[a-zA-Z]{2,6})\b/i","<span><a href=\"mailto://$1\" class=\"twitter-link\">$1</a></span>", $text);
	
	    //mach #trendingtopics. Props to Michael Voigt
	    $text = preg_replace('/([\.|\,|\:|\?|\?|\>|\{|\(]?)#{1}(\w*)([\.|\,|\:|\!|\?|\>|\}|\)]?)\s/i', "$1<span><a href=\"http://twitter.com/#search?q=$2\" class=\"twitter-link\">#$2</a></span>$3 ", $text);
	
	    return $text;
	
	}
	
	function twitter_users($text) {
	       $text = preg_replace('/([\.|\,|\:|\?|\?|\>|\{|\(]?)@{1}(\w*)([\.|\,|\:|\!|\?|\>|\}|\)]?)\s/i', "$1<span>@<a href=\"http://twitter.com/$2\" class=\"twitter-user\">$2</a></span>$3 ", $text);
	       return $text;
	}     
	
}
