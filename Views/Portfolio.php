<div id="portfolio" class="page">

	<div class="container">
	
		<div class="sixteen columns">
			<h1>Portfolio</h1>
		</div>
		
		<!-- Start Filters -->
        <ul class="option-set" data-option-key="filter">
        	<li><i class="icon-reorder"></i></li>
	    	<li><a class="selected" href="#filter" data-option-value="*"><span></span>All projects</a></li>
			<li><a href="#filter" data-option-value=".photography"><span></span>Photography</a></li>
			<li><a href="#filter" data-option-value=".video"><span></span>Video</a></li>
			<?php /*<li><a href="#filter" data-option-value=".design"><span></span>Design</a></li>*/ ?>
    	</ul>
		<!-- End Filters -->
		
		
		<div id="portfolio_items" class="isotope">
			
			<!-- Start Project -->
			<div class="four columns item isotope-item video">
				<a href="http://www.youtube.com/embed/RAYaKLoxQ8g" id="promoVideo" class="fancybox-media" rel="group1">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/promo_vid.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-001.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-001.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-002.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-002.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-003.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-003.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-004.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-004.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-0005.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-005.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-006.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-006.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-007.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-007.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-008.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-008.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-009.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-009.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-010.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-010.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-011.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-011.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-012.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-012.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/new-img-013.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/new-img-013.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-001.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-001.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
			<!-- End Project -->

			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-002.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-002.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-003.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-003.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-004.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-004.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-005.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-005.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item video">
				<a href="http://www.youtube.com/embed/tDz9ypNFM7s" class="fancybox-media" rel="group1">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/wanted_dead_or_alive.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->	
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-006.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-006.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item video">
				<a href="http://www.youtube.com/embed/chn28pvDGBQ" class="fancybox-media" rel="group1">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/its_my_life.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-011.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-011.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-015.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-015.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-016.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-016.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-017.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-017.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item video">
				<a href="http://www.youtube.com/embed/EgtrtDVDG6g" class="fancybox-media" rel="group1">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/runaway.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-019.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-019.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-020.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-020.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-021.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-021.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-023.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-023.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-024.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-024.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-030.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-030.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-035.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-035.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-036.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-036.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-037.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-037.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-038.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-038.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
			<!-- Start Project -->
			<div class="four columns item isotope-item photography">
				<a href="/images/portfolio/img-039.jpg" class="fancybox-media" rel="group2">
					<div class="img-wrp">
				        <img class="scaleimg" src="/images/portfolio/thumbs/img-039.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>	
			<!-- End Project -->
			
		</div>
		
	</div> 	
	
</div>
