<div id="shows" class="page">
	
	<div class="container">
		<div class="sixteen columns">
			<h1>Shows</h1>
			<div class="one-text">
				<p>Ultimate Bon Jovi is a cohesion of five seasoned musicians who together recreate the songs and stylings of one of the worlds most popular and recognizable bands for nearly four decades. From the rock anthems to the love ballads, Ultimate Bon Jovi will take you back to 1984 and bring you back again. From "Runaway" to "Who Says You Can't Go Home". With veritable costuming, hairstyles, and personified performances, Ultimate Bon Jovi offers a true BON JOVI concert experience. Come join this bona fide salute to BON JOVI, both past and not so long ago.</p>
			</div>
		</div>
	</div>
	
	
	<!-- Start Full Width Video -->
	<div class="full-width">
		<div class="container">
			<div class="sixteen columns">
				<div class="embed-container">
					<iframe width="560" height="315" src="//www.youtube.com/embed/RAYaKLoxQ8g" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>

	
	<!-- Start Full Width Video -->
	<div class="full-width">
		<div class="container">
			<div class="sixteen columns">
				
				<section class="timeline-container">
					<h3>Show Dates</h3>
					<div class="timeline-container-box">
						<div id="my-timeline">
							<div class="timeline-html-wrap" style="display: none">
								
								<div class="timeline-event">
									<div class="timeline-date">06.23.2011</div>
									<div class="timeline-title">The Canyon Club</div>
									<div class="timeline-content">
										<p>The debut gig for <?=$common->siteName;?>. With Nik Jovi on vocals, you don't want to miss this show. All of the best songs from Bon Jovi are in the set lineup. Come party with us.</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/events/events" target="_blank">View All Events</a></div>
								</div>
								
								<div class="timeline-event">
									<div class="timeline-date">04.05.2012</div>
									<div class="timeline-title">The Blooze</div>
									<div class="timeline-content">
										<p>Come on out to The Blooze on the night of Cinco De Mayo!!! We will be sharing the stage with Breakdown the Tom Petty experience.. Bon Jovi and Petty same stage!!!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/events/events" target="_blank">View All Events</a></div>
								</div>
								
								<div class="timeline-event">
									<div class="timeline-date">04.14.2012</div>
									<div class="timeline-title">Martini Ranch</div>
									<div class="timeline-content">
										<p>Come out and see <?=$common->siteName;?> at Martini Ranch!!! Enjoy a night with your favorite Bon Jovi songs! The band promises a few new surprises... Be prepared to enjoy the rest of the night with Rock Lobster the Absolute Best Cover band in Arizona! when it comes to playing the Ultimate in 80?s Retro Music! See you there!!!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/events/events" target="_blank">View All Events</a></div>
								</div>
								
								<div class="timeline-event">
									<div class="timeline-date">06.14.2012</div>
									<div class="timeline-title">Martini Ranch</div>
									<div class="timeline-content">
										<p>Come out and see <?=$common->siteName;?> at Martini Ranch!!! Enjoy a night with your favorite Bon Jovi songs! The band promises a few new surprises... Be prepared to enjoy the rest of the night with Rock Lobster the Absolute Best Cover band in Arizona! when it comes to playing the Ultimate in 80's Retro Music! See you there!!!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/events/events" target="_blank">View All Events</a></div>
								</div>
								
								<div class="timeline-event">
									<div class="timeline-date">09.01.2012</div>
									<div class="timeline-title">Joes Grotto</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> for the 1st time @ Joes Grotto!!! Celebrate one night with, Jimi Hendrix and Black Crows tribute</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/events/events" target="_blank">View All Events</a></div>
								</div>
								
								<div class="timeline-event">
									<div class="timeline-date">09.15.2012</div>
									<div class="timeline-title">The Blooze</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> and Unskinny Bop, A tribute to Poison & beyond, take the stage at The Blooze. One night of high energy rock and roll... be there!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/events/events" target="_blank">View All Events</a></div>
								</div>
								
								<div class="timeline-event">
									<div class="timeline-date">03.09.2013</div>
									<div class="timeline-title">Martini Ranch</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> with special guest Justin Simison return to Martini Ranch.</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/events/events" target="_blank">View All Events</a></div>
								</div>
								
								<div class="timeline-event">
									<div class="timeline-date">05.10.2013</div>
									<div class="timeline-title">Martini Ranch</div>
									<div class="timeline-content">
										<p>We will be rocking out all night while Martini Ranch is celebrating 18 years of business with special guests Bad Reputation.</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/events/events" target="_blank">View All Events</a></div>
								</div>
								
								<div class="timeline-event">
									<div class="timeline-date">08.17.2013</div>
									<div class="timeline-title">Martini Ranch</div>
									<div class="timeline-content">
										<p>We have been working very hard on our next show, and are excited to bring you something NEW!!! Come join us as we rock the stage all night at Martini Ranch in Scottsdale.</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>
								
								<div class="timeline-event">
									<div class="timeline-date">09.14.2013</div>
									<div class="timeline-title">The Blooze</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> returns to The Blooze! There is nowhere cooler to be on a hot Arizona summer night than with us rockin' the stage. Come party with us!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

								<div class="timeline-event">
									<div class="timeline-date">10.26.2013</div>
									<div class="timeline-title">The Roxy Lounge</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> at The Roxy Lounge! Come out and finish the year off with last party till New Year's eve!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

								<div class="timeline-event">
									<div class="timeline-date">12.28.2013</div>
									<div class="timeline-title">The Roxy Lounge</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> at The Roxy Lounge! Come out and finish the year off with last party till New Year's eve!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

								<div class="timeline-event">
									<div class="timeline-date">03.08.2014</div>
									<div class="timeline-title">The Blooze Bar</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> at The Blooze Bar!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

								<div class="timeline-event">
									<div class="timeline-date">08.20.2014</div>
									<div class="timeline-title">The Blooze Bar</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> at The Blooze Bar!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

								<div class="timeline-event">
									<div class="timeline-date">08.30.2014</div>
									<div class="timeline-title">The Blooze Bar</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> at The Blooze Bar!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

								<div class="timeline-event">
									<div class="timeline-date">09.25.2015</div>
									<div class="timeline-title">Desert Ridge Marketplace</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> at Desert Ridge Marketplace!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

								<div class="timeline-event">
									<div class="timeline-date">09.26.2015</div>
									<div class="timeline-title">Tempe Marketplace</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> at Tempe Marketplace!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

								<div class="timeline-event">
									<div class="timeline-date">05.21.2016</div>
									<div class="timeline-title">The Blooze Bar</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> at The Blooze Bar!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

								<div class="timeline-event">
									<div class="timeline-date">05.22.2016</div>
									<div class="timeline-title">The Blooze Bar</div>
									<div class="timeline-content">
										<p><?=$common->siteName;?> at The Blooze Bar!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

								<div class="timeline-event">
									<div class="timeline-date">05.28.2016</div>
									<div class="timeline-title">Wasted Grain</div>
									<div class="timeline-content">
										<p>Enjoy an EPIC night with <?=$common->siteName;?> Memorial Day Weekend at Wasted Grain!</p>
										<p>Follow this and other events on FaceBook</p>
									</div>
									<div class="timeline-link"><a href="https://www.facebook.com/thebonjovitribute/events" target="_blank">View All Events</a></div>
								</div>

							</div>
						</div>
					</div>
				</section>					
				
			</div>	
		</div>	
	</div>
	<!-- End Full Width Video -->
	
	<div class="container">
	
		<div class="four columns">
			<h4>Set List</h4>
			<ul>
				<li>Lay your hands on me</li>
				<li>Born to be my baby</li>
				<li>Raise your hands</li>
				<li>You give love a bad name</li>
				<li>In and out of love</li>
				<li>Wild in the streets</li>
				<li>Runaway</li>
			</ul>
		</div>
		
		<div class="four columns">
			<h4>&nbsp;</h4>
			<ul>
				<li>Wanted dead or alive</li>
				<li>Bad medicine</li>
				<li>Its my life</li>
				<li>Have a nice day</li>
				<li>Living on a prayer</li>
				<li>Who says you cant go home</li>
				<li>Wild is the wind</li>
			</ul>
		</div>
		
		<div class="four columns">
			<h4>&nbsp;</h4>
			<ul>
				<li>These days</li>
				<li>Just older</li>
				<li>Let it rock</li>
				<li>Blood on blood</li>
				<li>In these arms</li>
				<li>Keep the faith</li>
				<li>I'll be there for you</li>
			</ul>
		</div>
			
		<div class="four columns">
			<h4>&nbsp;</h4>
			<ul>
				<li>Blaze of glory</li>
				<li>Someday i'll be Saturday night</li>
				<li>Bed of roses</li>
				<li>Superman tonight</li>
				<li>(do you want) to make a memory</li>
				<li>Lost highway</li>
			</ul>
		</div>
		
	</div>

</div>

<!-- Start Second Parallax Background (Twitter) -->
<div class="parallax">
    <div class="bg2"></div>
    <div class="pattern"></div>
    <div class="container">
        <div class="sixteen columns">
            <div class="tritter-card">
            	<h1>Follow us on Twitter</h1>
            	<?php
            	
				try {
					$response = $twitter->statusesUserTimeline(null, "ultimatebonjovi", null, 3);
					$twitter->twitter_messages($response, true);
				} catch (Exception $e) {
				    var_dump($e);
				}

            	?>
                <a href="http://www.twitter.com/ultimatebonjovi" target="_blank"><img alt="" src="images/social-icons/follow.png" style="margin-top: 36px;" /></a>
            </div>
        </div>
    </div>
</div>
<!-- End Second Parallax Background (Twitter) -->
