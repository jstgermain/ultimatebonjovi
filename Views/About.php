<div id="about" class="page">

	<div class="container">
		
		<div class="sixteen columns">
			<h1>About Ultimate Bon Jovi</h1>
		</div>
		
	</div>
	
	<div class="container bottom-padding">
	
		<div class="eight columns">
			<p>Back in a small town at the Jersey shore, Johnny Giovanni was working in a local pizza parlor owned by Nico. Johnny had dreams of forming a Bon Jovi tribute band, something to get out of the small town world, something bigger and better than he was. He had the look, could sing and play the guitar.</p>
			<p>He just needed the band. One day a guy with a guitar slung across his back came into the pizza place. His name was Ritchie Samborini. A slice and a Coke turned into a dream. Johnny told Ritchie about his dream of a band and how he'd been holding onto it. Ritchie said he was in. Now they needed the other half of the band.</p>
		</div>
		
		<div class="eight columns">
			<p>Nico overheard the conversations and said he's a drummer who had the chops to handle the job. Nico was interested.</p>
			<p>The last piece of the puzzle was the bassist and keyboard player. They needed just the right fit but didn't know who could pull it off until one day Bobby and Danny  strolled in for a slice. The were both classically trained and had studied at Juilliard in nearby New York City and came down the shore for the summer.</p>
			<p>That's how Ultimate Bon Jovi became Ultimate Bon Jovi.</p>
		</div> 
		
	</div>
	
	<div class="full-width" style="margin: 0 auto;">
		
		<div class="container">
			
			<!-- Start Profile -->
			<div class="seven columns">
				<h3>Nik Jovi</h3>
				<p class="job-position">Lead Singer</p> 
			    <div class="img-wrp">
	    			<img alt="" class="scaleimg" src="images/members/nik_jovi.jpg" />
	    			<div class="overlay-wrp">
	        			<div class="overlay"></div>
	        			<ul class="social-icons overlay-content">
							<li><a href="https://www.facebook.com/nikjovi" target="_blank"><i class="icon-facebook"></i></a></li>
							<li><a href="<?=$common->twitter;?>" target="_blank"><i class="icon-twitter"></i></a></li>
	        			</ul>
	    			</div>
			    </div>
				<?php /*<p class="description">Lorem ipsum dolor sit amet, consectetur <a href="#homepage">adipiscing elit</a>. Maecenas ac augue at erat hendrerit dictum. Praesent porta, purus eget sagittis imperdiet, nulla mi ullamcorper metus, id hendrerit metus diam vitae est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>*/ ?>
			</div>
			<!-- End Profile -->	
			
			<!-- Start Profile -->			
			<div class="seven columns offset-by-two">
				<h3>Tristan Mavraus</h3>
				<p class="job-position">Lead Guitar &amp; Vocals</p> 
			    <div class="img-wrp">
	    			<img alt="" class="scaleimg" src="images/members/tristan_mavraus.jpg" />
	    			<div class="overlay-wrp">
	        			<div class="overlay"></div>
	        			<ul class="social-icons overlay-content">
							<li><a href="https://www.facebook.com/tristan.koclanis" target="_blank"><i class="icon-facebook"></i></a></li>
	        			</ul>
	    			</div>
			    </div>
				<?php /*<p class="description">Phasellus a nisi urna, facilisis facilisis diam. Vivamus enim ligula, sollicitudin nec porttitor nec, commodo vel justo. Nunc in mattis ipsum. Mauris accumsan pretium diam, sit amet iaculis purus ullamcorper ac. Donec pulvinar metus a ipsum varius suscipit. Mauris ut nisi at tortor molestie rhoncus ultrices quis nisi.</p>*/ ?>
			</div>
			<!-- End Profile -->	
			
		</div>
		
	</div>
	
	<div class="full-width" style="margin: 0 auto;">
		
		<div class="container">
			
			<!-- Start Profile -->			
			<div class="four columns offset-by-one">
				<h3>Tom Higgins</h3>
				<p class="job-position">Drums &amp; Vocals</p> 
			    <div class="img-wrp">
	    			<img alt="" class="scaleimg" src="images/members/tom_higgins.jpg" />
	    			<div class="overlay-wrp">
	        			<div class="overlay"></div>
	        			<ul class="social-icons overlay-content">
							<li><a href="https://www.facebook.com/tom.higgins.9237" target="_blank"><i class="icon-facebook"></i></a></li>
	        			</ul>
	    			</div>
			    </div>
				<?php /*<p class="description">Phasellus a nisi urna, facilisis facilisis diam. Vivamus enim ligula, sollicitudin nec porttitor nec, commodo vel justo. Nunc in mattis ipsum. Mauris accumsan pretium diam, sit amet iaculis purus ullamcorper ac. Donec pulvinar metus a ipsum varius suscipit. Mauris ut nisi at tortor molestie rhoncus ultrices quis nisi.</p>*/ ?>
			</div>
			<!-- End Profile -->	
			
			<!-- Start Profile -->			
			<div class="four columns offset-by-one">
				<h3>Rob Erwin</h3>
				<p class="job-position">Bass Guitar &amp; Vocals</p> 
			    <div class="img-wrp">
	    			<img alt="" class="scaleimg" src="images/members/rob_erwin.jpg" />
	    			<div class="overlay-wrp">
	        			<div class="overlay"></div>
	        			<ul class="social-icons overlay-content">
							<li><a href="https://www.facebook.com/rob.erwin.927" target="_blank"><i class="icon-facebook"></i></a></li>
	        			</ul>
	    			</div>
			    </div>
				<?php /*<p class="description">Phasellus a nisi urna, facilisis facilisis diam. Vivamus enim ligula, sollicitudin nec porttitor nec, commodo vel justo. Nunc in mattis ipsum. Mauris accumsan pretium diam, sit amet iaculis purus ullamcorper ac. Donec pulvinar metus a ipsum varius suscipit. Mauris ut nisi at tortor molestie rhoncus ultrices quis nisi.</p>*/ ?>
			</div>
			<!-- End Profile -->	
			
			<!-- Start Profile -->			
			<div class="four columns offset-by-one">
				<h3>Dave Hecker</h3>
				<p class="job-position">Keyboard &amp; Vocals</p> 
			    <div class="img-wrp">
	    			<img alt="" class="scaleimg" src="images/members/dave_hecker.jpg" />
	    			<div class="overlay-wrp">
	        			<div class="overlay"></div>
	        			<ul class="social-icons overlay-content">
							<li><a href="https://www.facebook.com/dave.hecker.3" target="_blank"><i class="icon-facebook"></i></a></li>
	        			</ul>
	    			</div>
			    </div>
				<?php /*<p class="description">Phasellus a nisi urna, facilisis facilisis diam. Vivamus enim ligula, sollicitudin nec porttitor nec, commodo vel justo. Nunc in mattis ipsum. Mauris accumsan pretium diam, sit amet iaculis purus ullamcorper ac. Donec pulvinar metus a ipsum varius suscipit. Mauris ut nisi at tortor molestie rhoncus ultrices quis nisi.</p>*/ ?>
			</div>
			<!-- End Profile -->	
			
		</div>
		
	</div>
	
</div>

<!-- Start First Parallax Background (Quote) -->	
<div class="parallax">
	<div class="bg1"></div>
	<div class="pattern"></div>
	<div class="container">
		<div class="vertical-text">
			<p class="prlx-quote">Miracles happen everyday, change your perception of what a miracle is and you'll see them all around you.</p>
			<div class="prlx-author">Jon Bon Jovi</div>
		</div>
	</div>
</div>
<!-- End First Parallax Background (Quote) -->
