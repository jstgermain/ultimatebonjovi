<div id="homepage">
	
	<div class="container">
		<div class="sixteen columns">
			<img alt="" class="logo" src="images/logo.png" />
		</div>
		
		<div class="promo-video">
			<div class="four columns">
				<h3>Promotional Video</h3>
				<a href="http://www.youtube.com/embed/RAYaKLoxQ8g" id="promoVideo" class="fancybox-media" rel="group1">
					<div class="img-wrp">
				        <img class="scaleimg" src="images/portfolio/thumbs/promo_vid.jpg" />
				        <div class="overlay-wrp">
		    		        <i class="icon loupe overlay-content"></i>
		    		        <div class="overlay"></div>
				        </div>
					</div>
			    </a>
			</div>
		</div>
		
		<div class="slider-text">
			<?php /*<div class="sixteen columns">
				<div class="line"></div>
			</div>*/ ?>
			
			<div class="twelve columns">
				<div id="slidecaption"></div>
			</div>
			
			<div class="four columns">
				<a id="prevslide" class="load-item"><i class="icon-chevron-left"></i></a>
				<a id="nextslide" class="load-item"><i class="icon-chevron-right"></i></a>
			</div>
		</div>
		
	</div>
</div>
