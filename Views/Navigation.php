<nav>

	<div class="container">
		
		<div class="thirteen columns">
			
			<!-- Start Nav Links -->
			<ul id="nav" class="links">
				<li class="current"><a href="#about">About</a></li>
				<li><a href="#shows">Shows</a></li>
				<li><a href="#portfolio">Portfolio</a></li>
				<li><a href="#contact">Contact</a></li>
				<li><a class="to-top" href="#homepage"><i class="icon-chevron-up"></i><span>Home</span></a></li>
			</ul>
			<!-- End Nav Links -->
			
		</div>
		
		<div class="three columns">
		
			<!-- Social Icons -->	
			<ul class="social-icons">
				<li><a href="<?=$common->facebook;?>" target="_blank"><i class="icon-facebook"></i></a></li>
				<li><a href="<?=$common->twitter;?>" target="_blank"><i class="icon-twitter"></i></a></li>
				<li><a href="<?=$common->youtube;?>" target="_blank"><i class="icon-youtube"></i></a></li>
			</ul>
			<!-- End Icons -->
		
		</div>
	
	</div>

</nav>
