<div id="contact">
	<div class="pattern"></div>
	<div class="container">
		<div class="sixteen columns">
			
			<!-- Start Contact Block -->
			<div class="card">
				<h1 class="white">Contacts Us</h1>
				<div class="js-tabs">
					<ul class="contact-tabs">
						<li><a href="#" class="active-tab current">Contact</a></li>
						<?php /*<li><a href="#">Map</a></li>*/ ?>
						<li><a href="#">Feedback</a></li>
					</ul>
					
					<div class="tab-content-wrp">
    					<div class="lines tab-content active">
    						<p>For booking and questions, Contact us at:</p>
    						<p><a href="mailto:ultimatebonjovi@gmail.com" >ultimatebonjovi@gmail.com</a></p>
    						<p>480.343.6629</p>
    					</div>
    					
    					<?php /*<div class="lines tab-content">
    						<iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=8613+E+Solano+Dr,+Scottsdale,+AZ+85250&amp;aq=t&amp;sll=33.520771,-111.895752&amp;sspn=0.006762,0.013937&amp;ie=UTF8&amp;hq=&amp;hnear=8613+E+Solano+Dr,+Scottsdale,+Maricopa,+Arizona+85250&amp;t=m&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
    					</div>*/ ?>
    					
    					<div class="lines tab-content">
    						<div class="result"></div>
            				
            				<form class="well">
            					<div class="row">
            						<div class="span2 pull-left">
            							<input type="text" name="name" placeholder="Name">
            							<input type="text" name="email" placeholder="E-mail">
            							<input type="text" name="subject" placeholder="Subject">
            						</div>
            						<div class="span6 pull-right">
            							<textarea class="span6" rows="4" name="message" placeholder="Message"></textarea>
            							<button type="submit" class="button"><i class="icon-envelope-alt"></i> Send Message</button>
            						</div>
            					</div>
            				</form>
    					</div>
					</div>
				</div>
				
				<ul class="contact-social-icons">
					<li><a href="<?=$common->facebook;?>" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a href="<?=$common->twitter;?>" target="_blank"><i class="icon-twitter"></i></a></li>
					<li><a href="<?=$common->youtube;?>" target="_blank"><i class="icon-youtube"></i></a></li>
				</ul>
			</div>
			<!-- End Contact Block -->
			
		</div>
	</div>

	<div class="copyright">
		<p>&copy; <?php echo date("Y"); ?> Ultimate Bon Jovi. Created by <a href="http://www.ibrightdev.com/" target="_blank">iBright Development</a>.</p>
	</div>		

</div>
